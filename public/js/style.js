function clearSession() {
    localStorage.removeItem('token');
    localStorage.removeItem('auth');
    this.name = localStorage.getItem('name');
    this.user_type = localStorage.getItem('user_type');
    this.auth = localStorage.getItem('auth');
    this.item = localStorage.getItem('item');
}

moveOnMax = function(field, nextFieldID) {
    if (field.value.length == 1) {
        document.getElementById(nextFieldID).focus();
    }
}






$(function() {
    // $('[data-decrease]').click(decrease);
    // $('[data-increase]').click(increase);
    $('[data-value]').change(valueChange);
});

function decrease() {
    var value = $(this).parent().find('[data-value]').val();
    if (value > 1) {
        value--;
        $(this).parent().find('[data-value]').val(value);
    }
}

function increase() {
    var value = $(this).parent().find('[data-value]').val();
    if (value < 100) {
        value++;
        $(this).parent().find('[data-value]').val(value);
    }
}

function valueChange() {
    var value = $(this).val();
    if (value == undefined || isNaN(value) == true || value <= 0) {
        $(this).val(1);
    } else if (value >= 101) {
        $(this).val(100);
    }
}