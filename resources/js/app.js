import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import VueLoading from 'vuejs-loading-plugin'
import VueTelInput from 'vue-tel-input';
import JwPagination from 'jw-vue-pagination';
import shareIt from 'vue-share-it';
import VueLodash from 'vue-lodash'
import lodash from 'lodash'

import GoogleAuth from 'vue-google-oauth2'
const gauthOption = {
    clientId: '30173417980-1mr78rb4ietprqboovvm5j9hg3265fj2.apps.googleusercontent.com',
    scope: 'profile email',
    prompt: 'select_account',
};
Vue.use(GoogleAuth, gauthOption);
Vue.use(VueLodash, { name: 'custom', lodash: lodash })

Vue.use(shareIt);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueLoading)
Vue.use(VueToast);
Vue.use(VueTelInput);
Vue.component('jw-pagination', JwPagination);


import App from './view/App'
import Index from './pages/index'
import Products from './pages/products'
import Product from './pages/product'
import Invoice from './pages/invoice'
import Card from './pages/card'
import Support from './pages/support'
import Categories from './pages/categories'
import Product_Cat from './pages/product_cat'
import Policy from './pages/policy'
import Home from './pages/home'
import Profile from './pages/profile'
import Password from './pages/password'


const router = new VueRouter({
    mode: 'history',
    routes: [{
            path: '/',
            name: 'index',
            component: Index
        },
        {
            path: '/products',
            name: 'products',
            component: Products
        },
        {
            path: '/product/:id',
            name: 'product',
            component: Product
        },
        {
            path: '/invoice/:id',
            name: 'invoice',
            component: Invoice
        },
        {
            path: '/card/:id',
            name: 'card',
            component: Card
        },
        {
            path: '/support',
            name: 'support',
            component: Support
        },
        {
            path: '/categories',
            name: 'categories',
            component: Categories
        },
        {
            path: '/products/category/:id',
            name: 'products_category',
            component: Product_Cat
        },
        {
            path: '/policy',
            name: 'policy',
            component: Policy
        },
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },
        {
            path: '/password',
            name: 'password',
            component: Password
        },

    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,

});